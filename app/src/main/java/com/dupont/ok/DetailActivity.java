package com.dupont.ok;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

import com.dupont.ok.fragments.DetailFragment;

public class DetailActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    DetailFragment detailFragment = new DetailFragment();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putString("memo", getIntent().getExtras().getString("memo"));
        detailFragment.setArguments(bundle);

        fragmentTransaction.replace(R.id.memo_detail, detailFragment, "detail");

        fragmentTransaction.commit();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Log.i("config", "changement de config");

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            finish();
        }
    }
}
