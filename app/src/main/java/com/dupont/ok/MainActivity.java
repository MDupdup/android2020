package com.dupont.ok;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;
import com.dupont.ok.fragments.DetailFragment;
import com.dupont.ok.fragments.MemoFragment;


public class MainActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadMemoFragment();
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        Toast.makeText(this, "" + preferences.getInt("position", -1), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        setContentView(R.layout.activity_main);


        Log.i("config", "changement de config");

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            loadDetailFragment("Cliquez sur un item pour voir sa description");
            loadMemoFragment();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            loadMemoFragment();
        }
    }

    public void loadMemoFragment() {


        MemoFragment memoFragment = new MemoFragment();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.replace(R.id.memo_fragment, memoFragment, "memo");

        fragmentTransaction.commit();
    }

    public void loadDetailFragment(String value) {
        //DetailFragment detailFragment = (DetailFragment) fragmentManager.findFragmentById(R.id.memo_detail_fragment);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Bundle bundle = new Bundle();
        bundle.putString("memo", value);

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.memo_detail_fragment, detailFragment, "detail");


        fragmentTransaction.commit();
    }
}
