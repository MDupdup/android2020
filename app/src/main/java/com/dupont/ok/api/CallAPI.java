package com.dupont.ok.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.dupont.ok.api.Zoup;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

public class CallAPI {
    private static final String REMOTE_URL = "http://httpbin.org/post";

    private static final AsyncHttpClient client = new AsyncHttpClient();

    private static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            return networkInfo.isConnected();
        }
        return false;
    }

    public static void post(String label, Context c) {
        if (!isConnected(c)) {
            Log.e("okpulco", "No active Connexion!");

            Toast.makeText(c, "", Toast.LENGTH_LONG).show();

            return;
        }

        RequestParams params = new RequestParams();
        params.put("label", label);

        client.post(REMOTE_URL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String res = new String(responseBody);

                Zoup zoup = new Gson().fromJson(res, Zoup.class);

                Log.i("okpulco", zoup.getForm().getLabel());

                Toast.makeText(c, zoup.getForm().getLabel(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("okpulco", error.toString());
            }
        });
    }
}
