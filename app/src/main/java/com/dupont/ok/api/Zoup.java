package com.dupont.ok.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Form {
    @SerializedName("label")
    @Expose
    private String label;

    public String getLabel() {
        return label;
    }

}

public class Zoup {
    @SerializedName("form")
    @Expose
    private Form form;

    Form getForm() {
        return form;
    }

}