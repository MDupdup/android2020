package com.dupont.ok.database;

import com.dupont.ok.models.Memo;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public abstract class MemoDAO {

    @Query("SELECT * FROM memos")
    public abstract List<Memo> getMemoList();

    @Insert
    public abstract void insertMemo(Memo... memos);

    @Update
    public abstract void updateMemo(Memo... memos);

    @Delete
    public abstract void deleteMemo(Memo... memos);

    @Query("SELECT * FROM memos WHERE memoId=1")
    public abstract Memo getMemoById();
}
