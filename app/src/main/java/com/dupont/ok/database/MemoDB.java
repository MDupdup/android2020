package com.dupont.ok.database;

import com.dupont.ok.models.Memo;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Memo.class}, version = 1)
public abstract class MemoDB extends RoomDatabase {

    public abstract MemoDAO memoDAO();
}
