package com.dupont.ok.database;

import android.content.Context;

import androidx.room.Room;

public class MemoDBHelper {

    private static MemoDBHelper dbHelper = null;
    private MemoDB db;

    private MemoDBHelper(Context c) {
        db = Room
                .databaseBuilder(c, MemoDB.class, "memos.db")
                .allowMainThreadQueries()
                .build();
    }

    public static synchronized MemoDB getDB(Context c) {
        if (dbHelper == null) {
            dbHelper = new MemoDBHelper(c.getApplicationContext());
        }
        return dbHelper.db;
    }
}
