package com.dupont.ok.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dupont.ok.R;
import com.dupont.ok.api.CallAPI;
import com.dupont.ok.database.MemoDAO;
import com.dupont.ok.database.MemoDBHelper;
import com.dupont.ok.models.Memo;
import com.dupont.ok.recyclerview.ItemTouchHelperCallback;
import com.dupont.ok.recyclerview.MainAdapter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MemoFragment extends Fragment {

    private TextView detailText;
    public EditText input_text;
    public RecyclerView recyclerView;
    public Button btn;

    List<Memo> memoList = new ArrayList<>();
    public MemoDAO db;


    public MemoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_memo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Context ctx = this.getContext();

        input_text = view.findViewById(R.id.text_input);
        recyclerView = view.findViewById(R.id.recycler_view_main);
        btn = view.findViewById(R.id.ok_button);

        db = MemoDBHelper.getDB(ctx).memoDAO();
        updateList();

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(ctx);
        recyclerView.setLayoutManager(layoutManager);
        MainAdapter adapter = new MainAdapter(memoList, ctx);
        recyclerView.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelperCallback(adapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        btn.setOnClickListener(v -> {
            Memo newMemo = new Memo(input_text.getText().toString(), false);
            db.insertMemo(newMemo);
            memoList.add(newMemo);

            input_text.setText("");
            updateList();
            adapter.notifyDataSetChanged();

            Toast.makeText(ctx, "Successfully added an item in your list!", Toast.LENGTH_LONG).show();
        });

//        detailText = view.findViewById(R.id.detail_text);
//
//        detailText.setText(memo.getLabel());
    }

    private void updateList() {
        memoList = db.getMemoList();
    }

}
