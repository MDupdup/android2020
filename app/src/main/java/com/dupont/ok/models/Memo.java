package com.dupont.ok.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "memos")
public class Memo {

    @PrimaryKey(autoGenerate = true)
    private long memoId = 0;
    private String label;
    private boolean isDone;

    public Memo() {}

    public Memo(String label, boolean isDone) {
        this.label = label;
        this.isDone = isDone;
    }

    public long getMemoId() {
        return memoId;
    }

    public void setMemoId(long memoId) {
        this.memoId = memoId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
}
