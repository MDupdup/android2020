package com.dupont.ok.recyclerview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.dupont.ok.DetailActivity;
import com.dupont.ok.MainActivity;
import com.dupont.ok.R;
import com.dupont.ok.api.CallAPI;
import com.dupont.ok.database.MemoDAO;
import com.dupont.ok.database.MemoDB;
import com.dupont.ok.database.MemoDBHelper;
import com.dupont.ok.models.Memo;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MainAdapter extends RecyclerView.Adapter<MainViewHolder> {

    private List<Memo> memoList;
    private Context context;
    private MemoDB db;

    public MainAdapter(List<Memo> memoList, Context c) {
        this.memoList = memoList;
        this.context = c;

        db = MemoDBHelper.getDB(c);
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MainViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.memo_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
        holder.rvItemTextView.setText(memoList.get(position).getLabel());

        holder.rvItemTextView.setOnClickListener(v -> {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.context);
            @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("position", position);
            editor.apply();

            String label = memoList.get(position).getLabel();

            CallAPI.post(label, context);

            if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                MainActivity main = (MainActivity) context;
                main.loadDetailFragment(label);
            } else {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("memo", label);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return memoList.size();
    }

    public boolean onItemMove(int startPos, int endPos) {
        Collections.swap(memoList, startPos, endPos);
        notifyItemMoved(startPos, endPos);
        return true;
    }

    public void onItemDismiss(int pos) {
        if (pos > -1) {
            db.memoDAO().deleteMemo(memoList.get(pos));
            memoList.remove(pos);
            notifyItemRemoved(pos);
        }
    }
}
