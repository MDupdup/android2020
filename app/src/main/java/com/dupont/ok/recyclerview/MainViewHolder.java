package com.dupont.ok.recyclerview;

import android.view.View;
import android.widget.TextView;

import com.dupont.ok.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class MainViewHolder extends RecyclerView.ViewHolder {

    TextView rvItemTextView;

    MainViewHolder(@NonNull View itemView) {
        super(itemView);
        rvItemTextView = itemView.findViewById(R.id.recycler_view_item_text);
    }
}

